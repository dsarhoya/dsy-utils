package cl.dsarhoya.dsyutils.db;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by capi on 21/04/17.
 */

public class MigrationManager {

    private List<Migration> migrations = new ArrayList<>();

    public void registerMigration(Migration migration){
        migrations.add(migration);
    }

    public void applyMigration(Database db, int oldVersion, int newVersion){

        for (Migration migration : migrations){
            if (migration.getMigratedVersion() == newVersion){
                migration.applyMigration(db, oldVersion);
                break;
            }
        }
    }
}
