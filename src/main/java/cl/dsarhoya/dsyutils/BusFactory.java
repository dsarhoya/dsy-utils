package cl.dsarhoya.dsyutils;

import com.squareup.otto.Bus;

/**
 * Created by capi on 27/01/17.
 */

public class BusFactory {
    private static final Bus BUS = new Bus();

    public static Bus getBus(){
        return BUS;
    }
}
