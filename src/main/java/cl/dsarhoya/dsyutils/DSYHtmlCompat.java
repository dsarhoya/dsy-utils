package cl.dsarhoya.dsyutils;

import android.text.Html;
import android.text.Spanned;

/**
 * Created by capi on 23/05/17.
 */

public class DSYHtmlCompat {

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
