package cl.dsarhoya.dsyutils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by cpine on 26/10/2017.
 */

public class FormatUtils {

    public static String reformatDate(String date, String originalFormat, String finalFormat) throws ParseException {
        SimpleDateFormat originalFormatSDF = new SimpleDateFormat(originalFormat);
        SimpleDateFormat finalFormatSDF = new SimpleDateFormat(finalFormat);

        return finalFormatSDF.format(originalFormatSDF.parse(date));
    }
}
