package cl.dsarhoya.dsyutils.view;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.dsarhoya.dsyutils.R;


/**
 * Created by capi on 18/02/17.
 */

public abstract class SelectablesGridAdapter extends ArrayAdapter<SelectablesGridAdapter.SelectableObject> {

    public SelectablesGridAdapter(Context context, List<SelectableObject> objects) {
        super(context, android.R.layout.simple_spinner_item, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = convertView;

        if (v == null){
            v = inflater.inflate(getLayout(), parent, false);
        }

        SelectableObject selectable = getItem(position);

        setViewBaseSetup(v, selectable);

        if (selectable.isSelected())
            showSelected(v);
        else
            showUnselected(v);


        return v;
    }

    public void toggleSelection(int position, View view){

        SelectableObject selected = getItem(position);

        if (selected.isSelected()){
            showUnselected(view);
            selected.setSelected(false);

        }else{
            showSelected(view);
            selected.setSelected(true);
        }

    }

    public List<SelectableObject> getSelecteds(){
        List<SelectableObject> selecteds = new ArrayList<>();

        for (int i = 0; i < getCount(); i++){
            SelectableObject s = getItem(i);
            if (s.isSelected()){
                selecteds.add(s);
            }
        }

        return selecteds;
    }

    abstract protected void showSelected(View view);
    abstract protected void showUnselected(View view);
    abstract protected int getLayout();

    protected void setViewBaseSetup(View v, SelectableObject selectable){
        TextView labelTV = (TextView) v.findViewById(R.id.text);
        labelTV.setText(selectable.getDisplayText());
    }

    static public class SelectableObject{
        private String displayText;
        private Object optionObject;
        private boolean selected;

        public SelectableObject(String displayText, Object optionObject) {
            this.displayText = displayText;
            this.optionObject = optionObject;
        }

        public String getDisplayText() {
            return displayText;
        }


        public Object getOptionObject() {
            return optionObject;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public boolean getSelected(){
            return selected;
        }

    }
}
