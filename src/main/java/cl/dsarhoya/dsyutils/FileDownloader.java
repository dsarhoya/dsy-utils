package cl.dsarhoya.dsyutils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


/**
 * Created by kito on 06-01-17.
 */

public class FileDownloader extends AsyncTask<Void, Void, Void> {

    private Context context;
    private File directory;
    private String fileURL;
    private String fileName;
    private File targetFile;

    public FileDownloader(Context ctx, String url, String fileName){
        this.context = ctx;
        this.fileURL = url;
        this.fileName = fileName;
        this.directory = context.getCacheDir();
        this.targetFile = new File(directory, fileName);
        targetFile.getParentFile().mkdirs();
    }

    @Override
    protected Void doInBackground(Void... params) {

        return null;
    }

    public void downloadFile(){
        try{
            int count;
            URL url = new URL(fileURL);
            URLConnection connection = url.openConnection();
            connection.connect();

            int file_size = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(),8192);
            OutputStream output = new FileOutputStream(targetFile.getAbsoluteFile());

            byte data[] = new byte[1024];
            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (IOException e){
            Log.e(getClass().getSimpleName(), e.getMessage(), e);
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        BusFactory.getBus().post(this);
    }

    public File getDownloadedFile(){
        return targetFile;
    }
}
