package cl.dsarhoya.dsyutils.ws;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;

/**
 * Created by cpine on 24/10/2017.
 */

public abstract class HTTPRequestExceptionHandler {

    private Context context;

    public HTTPRequestExceptionHandler( @NonNull Context context) {
        this.context = context;
    }

    public void handle(Exception ex){

        String userReponse = null;

        if (ex instanceof ResourceAccessException){
            userReponse = onUnreachableHostError((ResourceAccessException) ex);
        }

        if (ex instanceof HttpStatusCodeException){
            HttpStatusCodeException clientException = (HttpStatusCodeException) ex;

            if (clientException.getStatusCode().value() == 404){
                userReponse = onNotFoundError(clientException);
            } else if (clientException.getStatusCode().value() == 401) {
                userReponse = onUnauthorizedError(clientException);
            }else if (clientException.getStatusCode().value() == 500){
                userReponse = onServerError(clientException);
            }else {
                userReponse = onAnotherError(clientException);
            }
        }

        showMessage(userReponse);
    }

    public void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public abstract String onUnreachableHostError(ResourceAccessException ex);
    public abstract String onNotFoundError(HttpStatusCodeException ex);
    public abstract String onServerError(HttpStatusCodeException ex);
    public abstract String onUnauthorizedError(HttpStatusCodeException ex);
    public abstract String onAnotherError(HttpStatusCodeException ex);

}
