package cl.dsarhoya.dsyutils.ws;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializer;
import com.squareup.otto.Bus;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import cl.dsarhoya.dsyutils.BusFactory;

/**
 * Created by capi on 26/01/17.
 *
 * Oportunidades de mejora:
 *
 * - Cambiar onSuccessResponse y onSuccessRawResponse por: onSuccessEntityResponse, onSuccessJsonResponse, onSuccessRawResponse (String), onSuccessEmptyResponse (204 status code)
 *
 * - onDependencyResolved
 */

public abstract class DSYRequest<T>  extends AsyncTask<String, Void, T> {

    private ArrayList<DSYRequest> dependencies = new ArrayList<DSYRequest>();
    private List<String> consumedUrls = new ArrayList<>();
    private Exception exception;
    private boolean executed;
    private HttpStatus statusCode;


    private boolean debugging;

    private JsonElement rawResponse;

    public void setDebugging(boolean debugging) {
        this.debugging = debugging;
    }

    protected RestTemplate getRestTemplate(){

        RestTemplate rt = new RestTemplate();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        rt.setRequestFactory(requestFactory);

        JsonSerializer serializer = getCustomSerializer();

        if (null != serializer){
            rt.getMessageConverters().clear();
            GsonHttpMessageConverter converter = new GsonHttpMessageConverter();
            Gson gson = new GsonBuilder().registerTypeAdapter(getResponseClass(), serializer).create();
            converter.setGson(gson);
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            rt.getMessageConverters().add(converter);
        }

        return rt;

    }

    protected JsonSerializer getCustomSerializer(){
        return null;
    }

    public List<String> getConsumedUrls() {
        return consumedUrls;
    }

    public boolean resolveDependencies(List<String> excludeRequests){

        for (DSYRequest dependency : dependencies) {


            if (dependencyExecuted(excludeRequests, dependency))
                continue;

            if (!dependency.resolveDependencies(excludeRequests)){
                exception = dependency.getException();
                return false;
            }

            consumedUrls.addAll(dependency.getConsumedUrls());

            dependency.sendRequest();


            if (dependency.isFailed()) {
                exception = dependency.getException();
                return false;
            }

            consumedUrls.add(dependency.getURL());
        }

        return true;
    }

    private boolean dependencyExecuted(List<String> executedUrls, DSYRequest dependency){

        if (executedUrls != null){
            for (String excludedUrl : executedUrls){
                if (excludedUrl.equals(dependency.getURL())){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    protected T doInBackground(String... params) {

        if (!resolveDependencies(null))
            return null;

        return this.sendRequest();
    }

    protected void addDependency(DSYRequest dependency){
        dependencies.add(dependency);
    }

    protected GsonBuilder getGsonBuilder(){
        GsonBuilder gBuilder = new GsonBuilder();
        gBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gBuilder;
    }

    protected HttpEntity getHttpEntity(){
        T entity = getEntity();

        if (entity == null){
            return new HttpEntity(getFormData(), getHttpHeader());
        }else{
            return new HttpEntity(entity, getHttpHeader());
        }
    }

    protected T sendRequest(){
        RestTemplate rt = getRestTemplate();
        T response = null;

        if (debugging){
            Log.d(getClass().getSimpleName(), "-- DSYRequest Debugging -- ");
            Log.d(getClass().getSimpleName(), "Url: " + getURL());
        }

        try {
            ResponseEntity<String> re = rt.exchange(getURL(),
                    getHttpMethod(),
                    getHttpEntity(),
                    String.class
            );

            statusCode = re.getStatusCode();

            if (debugging){
                logRequestResponse(statusCode, re.getBody());

            }



            if (null != getResponseClass() ) {

                if (re.getStatusCode().value() != 204){
                    Gson gson = getGsonBuilder().create();
                    response = gson.fromJson(re.getBody(), getResponseClass());
                }
                response = onSuccessResponse(response);
            }else{
                JsonParser parser = new JsonParser();
                rawResponse = parser.parse(re.getBody());
                onSuccessRawRespone(rawResponse);
            }
        }
        catch (HttpStatusCodeException e){
            exception = e;
            statusCode = e.getStatusCode();
            if (debugging){
                logRequestResponse(statusCode, e.getResponseBodyAsString());
            }
            onError(e);
        }
        catch (Exception e){
            exception = e;
            onError(e);
        } finally {
            executed = true;

            if (debugging){
                Log.d(getClass().getSimpleName(), "-- End DSY Request Debugging -- ");
            }
        }

        return response;
    }


    @Override
    protected void onPostExecute(T response) {

        if (isCancelled())
            return;

        Bus bus = getBus();

        if (null != bus){
            if (exception != null){
                bus.post(exception);
                return;
            }
            if (response != null)
                bus.post(response);

            if (rawResponse != null)
                bus.post(rawResponse);
        }

    }

    protected void onError(Exception e){
        if (e instanceof HttpStatusCodeException){
            Log.e(this.getClass().getSimpleName(),((HttpStatusCodeException) e).getResponseBodyAsString(), e);
        }else{
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }
    }

    private void logRequestResponse(HttpStatus statusCode, String body){
        Log.d(getClass().getSimpleName(), "Status Code: " + Integer.toString(statusCode.value()));
        if (body != null){
            Log.d(getClass().getSimpleName(), "Body: ");
            Log.d(getClass().getSimpleName(), body);
        }
    }

    public Exception getException() {
        return exception;
    }

    public boolean isExecuted(){return executed;}

    public HttpStatus getStatusCode(){
        return statusCode;
    }

    public boolean isFailed(){
        return exception != null;
    }



    protected MultiValueMap<String, Object> getFormData(){
        return null;
    }

    protected Bus getBus(){
        return BusFactory.getBus();
    }

    protected T onSuccessResponse(T response){
        return response;
    }

    protected JsonElement onSuccessRawRespone(JsonElement response){
        return response;
    }

    protected T getEntity(){
        return null;
    }

    abstract protected String getURL();
    abstract protected Class<T> getResponseClass();
    abstract protected HttpMethod getHttpMethod();
    abstract protected HttpHeaders getHttpHeader();





}
