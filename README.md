## Adding DSY Utils ##

```bash
git submodule add git@bitbucket.org:dsarhoya/dsy-utils.git
```

In settings.gradle include dsy utils:

```
include ':app', ':dsy-utils'
```

Adds dsy-utils as dependency:

```
compile project(':dsy-utils')
```